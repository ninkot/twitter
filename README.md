# README

## Overview

This is my simple Rails 5 application, created to learn Ruby on Rails.
I got inspiried with Twitter functionalities like:

* Authentication (using __devise__ gem)

* Creating tweets and view dashboard

* Friends mechanism (using __has_friendship__ gem)

* Likes mechanism (using __acts_as_votable__ gem)

## First Run

Clone this repo to your workspace
```bash
git clone https://ninkot@bitbucket.org/ninkot/twitter.git
```
Change directory to repo parent dir
```bash
cd twitter/
```
Install dependencies
```bash
bundle install
```
Set up your database
```bash
rails db:migrate
rails db:seed
```
Now you can run app server
```bash
rails s
```
Open your browser and type __localhost:3000__

Enjoy! :)

## Technologies used

* rails 5
* sqlite3
* haml
* scss
* coffeescript
* bootstrap 3
* rspec


## Feedback

If you are viewing my app, please give me a feedback. Nice to hear your suggestion for future developement :)
