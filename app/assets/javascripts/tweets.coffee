# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$(document).on 'ready page:load', ->
  $('.tweet-like').on 'ajax:success', (e, data) ->
    if data.status == 'success'
      $(this).find('.likes-count').text(data.likes_count)
