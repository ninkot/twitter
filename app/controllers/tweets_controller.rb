class TweetsController < ApplicationController
  before_action :find_tweet, except: [:index, :create]

  def index
    @tweets = model.all.order(created_at: :desc)
  end

  def create
    @tweet = model.new(tweet_params)
    @tweet.save
    redirect_to root_path
  end

  def edit
  end

  def update
    @tweet.update_attributes(tweet_params)
    redirect_to root_path
  end

  def destroy
    @tweet.destroy
    redirect_to root_path
  end

  def like
    unless @tweet.get_likes.exists?(voter: current_user)
      @tweet.liked_by current_user
    else
      @tweet.unliked_by current_user
    end
    render json: {status: 'success', likes_count: @tweet.get_likes.count}
  end

  private

  def find_tweet
    @tweet = model.find(params[:id])
  end

  def model
    current_user.tweets
  end

  def tweet_params
    params.require(:tweet).permit(:content)
  end

end
