module FriendsHelper
  def friends_for_user
    friends = current_user.friends
    render 'layouts/friend_list', friends: friends
  end
end
