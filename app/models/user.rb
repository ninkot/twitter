class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  has_many :tweets
  has_friendship
  validates :login, presence: true, uniqueness: true, format: { without: /.*admin.*/i }
  acts_as_voter
end
