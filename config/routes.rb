Rails.application.routes.draw do
  devise_for :users
  root to: 'tweets#index'
  resources :friends
  resources :users do
    resources :tweets do
      put :like, on: :member
    end
  end
end
